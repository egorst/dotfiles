set nocompatible
set t_Co=256
let g:zenburn_high_Contrast = 0
"colorscheme xoria256
"colorscheme xoria256 " zenburn xoria256
set autoindent          " always set autoindenting on
set smartindent
set expandtab
set smarttab
set softtabstop=4
set shiftwidth=4

"set number

set incsearch
nmap <silent> ,/ :let @/=""<CR> " clear highlighted search

map <C-Up> <C-Y><Up>
map <C-Down> <C-E><Down>
map <PageUp> <C-U><C-U>
map <PageDown> <C-D><C-D>
imap <PageUp> <C-O><C-U><C-O><C-U>
imap <PageDown> <C-O><C-D><C-O><C-D>

inoremap <C-h> <Left>|cnoremap <C-h> <Left>|
inoremap <C-k> <Up>|cnoremap <C-k> <Up>|
inoremap <C-j> <Down>|cnoremap <C-j> <Down>|
inoremap <C-l> <Right>|cnoremap <C-l> <Right>|

nnoremap <silent> ,F :let word=expand("<cword>")<CR>:vsp<CR>:wincmd w<cr>:exec("tag ". word)<cr>

" in gnome-terminal control-space generates ^@
map <C-@> <Esc>
map! <C-@> <Esc>

nnoremap ; :|xnoremap ; :

inoremap kj <ESC>
inoremap jj <ESC>

syntax on
set hlsearch
set smartcase
set showtabline=2
filetype on
filetype plugin on
filetype indent on

set scrolloff=2
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
set backspace=indent,eol,start
set relativenumber
set undodir="/tmp/vimdir"
set undofile

nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>

nnoremap <tab> %
vnoremap <tab> %

nnoremap <leader>r :set relativenumber!<cr>
nnoremap <leader>p :set paste! paste?<cr>

set wrap
"set textwidth=120
"set formatoptions=qrn1
"set colorcolumn=128

" set listcar but not list (set list manually)
set listchars=tab:▸\ ,eol:¬

nnoremap j gj
nnoremap k gk

set viminfo='10,\"100,:20,%

autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \  exe "normal! g`\"" |
  \ endif
